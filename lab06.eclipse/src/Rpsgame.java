//Phan Hieu Nghia - 1834104

import java.util.*;

public class Rpsgame {
	//private fields
	private int wins = 0;
	private int ties = 0;
	private int losses = 0;
	
	//Getter methods
	public int getWins() {
		return this.wins;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public int getLosses() {
		return this.losses;
	}
	
	//playRound method
	public String playRound(String playerChoice) {
		Random rand = new Random();
		int randChoice = rand.nextInt(3);
		
		
		String pcChoice = "";
		String winner = "";
		
		
		if(randChoice == 0) {
			pcChoice = "Rock";
		} else if (randChoice == 1) {
			pcChoice = "Paper";
		} else if (randChoice == 2) {
			pcChoice = "Scissors";
		}
		
		if(!playerChoice.equalsIgnoreCase(pcChoice)) {
			if(playerChoice.equalsIgnoreCase("Rock") && pcChoice.equalsIgnoreCase("Scissors")) {
				winner = "The user plays rock and the user won!";
				this.wins++;
			} else if (playerChoice.equalsIgnoreCase("Paper") && pcChoice.equalsIgnoreCase("Rock")) {
				winner = "The user plays paper and the user won!";
				this.wins++;
			} else if (playerChoice.equalsIgnoreCase("Scissors") && pcChoice.equalsIgnoreCase("Paper")) {
				winner = "The user plays scissors and the user won";
				this.wins++;
			} else {
				this.losses++;
				winner = "The computer won! You suck lel!";
			} 
		}
		else {
			winner = "The result are ties! There are no winner!";
			this.ties++;
		}
		return winner;
	}
}
