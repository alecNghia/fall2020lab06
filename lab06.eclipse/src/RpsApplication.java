//Phan Hieu Nghia - 1834104

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	private Rpsgame obj = new Rpsgame();
	
	public void start(Stage stage) {
		Group root = new Group(); 
		
		VBox overall = new VBox();
		
		//Buttons HBox
		HBox buttons = new HBox();
		Button rock = new Button("Rock");
		Button paper = new Button("Paper");
		Button scissors = new Button("Scissors");
		buttons.getChildren().addAll(rock, paper, scissors);
		buttons.setAlignment(Pos.CENTER); // put the buttons in the center
		buttons.setSpacing(30);
		overall.getChildren().add(buttons);
		
		//TextField HBox
		HBox textfield = new HBox();
		TextField msg = new TextField("Welcome!");
		msg.setPrefWidth(250);
		
		HBox scores = new HBox();
		TextField wins = new TextField("Wins: " + obj.getWins());
		wins.setPrefWidth(100);
		TextField losses = new TextField("Losses: " + obj.getLosses());
		losses.setPrefWidth(100);
		TextField ties = new TextField("Ties: " + obj.getTies());
		ties.setPrefWidth(100);
		
		//Credit HBox
		HBox credit = new HBox();
		TextField cred = new TextField("By Phan Hieu Nghia");
		cred.setAlignment(Pos.CENTER);
		
		credit.getChildren().add(cred);
		credit.setAlignment(Pos.CENTER);
		
		//Adds all the elements to the GUI and make things look nice
		textfield.getChildren().add(msg);
		textfield.setAlignment(Pos.CENTER); // put the message in the center
		
		scores.getChildren().addAll(wins, losses, ties);
		scores.setAlignment(Pos.CENTER); // put the score in the center
		
		overall.getChildren().addAll(textfield, scores, credit);
		overall.setAlignment(Pos.CENTER); // center everything
		
		root.getChildren().add(overall);

		
		//Make the buttons work when the player click on them
		RpsChoice rockChoice = new RpsChoice("Rock", msg, wins, losses, ties, obj);
		rock.setOnAction(rockChoice);
		RpsChoice paperChoice = new RpsChoice("Paper", msg, wins, losses, ties, obj);
		paper.setOnAction(paperChoice);
		RpsChoice scissChoice = new RpsChoice("Scissors", msg, wins, losses, ties, obj);
		scissors.setOnAction(scissChoice);

		
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 300, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

