import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;


//Phan Hieu Nghia - 1834104

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField msg;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private Rpsgame obj;
	
	
	public RpsChoice(String playerChoice, TextField msg, TextField wins, TextField losses, TextField ties, Rpsgame obj) {
		this.playerChoice = playerChoice;
		this.msg = msg;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.obj = obj;
	}
	
	public void handle(ActionEvent arg) {
		msg.setText(obj.playRound(playerChoice));
	
		wins.setText("Wins: " + obj.getWins() );
		losses.setText("Losses: " + obj.getLosses());
		ties.setText("Ties: " + obj.getTies());
	}
}
